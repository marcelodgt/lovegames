--set font size to 50
font = love.graphics.newFont(50)

function love.load()
  
  activeMenu = true
  rolls = 1
  numberOfPlayers = ''
  x = 100
  i = 1

  -- variables to check dice results and dice positions
  shipCount = 0
  shipPosition = ''
  captainCount = 0
  captainPosition = ''
  crewCount = 0
  crewPosition = ''
  numOfDice = ''

  -- player, dice and die structures
  player = {}
  player.dice = {}
  resultText = ''
  player.rollDie = function()
    die = {}
    die.value = love.math.random(1,6)
    die.x = x + (50 * i)
    i = i + 1
    table.insert(player.dice, die)
  end
end

function love.keyreleased(key)
  if key == "r" then
    --roll 5 dices
    for i=0, 4, 1 do
      if table.getn(player.dice) < 5 then
        player.rollDie()
      end
    end
    checkDiceResult(player.dice)
    -- removeDie(player.dice, shipCount, shipPosistion, captainCount, captainPosition, crewCount, crewPosition)
  end
end

function love.textinput(t)
  numberOfPlayers = numberOfPlayers .. t
end

function checkDiceResult(dices)
  for i,die in pairs (player.dice) do
    if die.value == 6 then
      shipCount = shipCount + 1
      shipPosition = i
    elseif die.value == 5 then
      captainCount = captainCount + 1
      captainPosition = i
    elseif die.value == 4 then
      crewCount = crewCount + 1
      crewPosition = i
    end
  end
end

-- function removeDie(dices, shipCount, captainCount, crewCount, shipPosition, captainPosition, crewPosition )
--   if(shipCount > 1) then
--     table.remove(player.dice, dice[shipPosition])
--     numOfDice =  table.getn(player.dice)
--   end
-- end

function love.update(dt)
  if love.keyboard.isDown(" ") then
    activeMenu = false;
  end
end

function love.draw()
  love.graphics.setFont(font)
  if(activeMenu) then
    love.graphics.print("Ship, Captain and Crew", 250, 50)
    love.graphics.print("Press space to begin", 400, 300)
  else
    love.graphics.print("Round:", 50, 150)
    love.graphics.print(rolls,240,150)
    -- love.graphics.print("How many players?", 50, 210)
    -- love.graphics.print(numberOfPlayers, 570, 210)
    love.graphics.print("Hit r to roll the dice", 300, 400)
    -- love.graphics.print(die.value, 100, 100)
  
    for _,die1 in pairs(player.dice) do
      love.graphics.print(die1.value, die1.x, 200)
     -- love.graphics.print(dice.text, 100, 300)
    end
  
    love.graphics.print(shipCount, 400,500)
    love.graphics.print(shipPosition, 480,500)
    love.graphics.print(captainCount, 400,550)
    love.graphics.print(captainPosition, 480,550)
    love.graphics.print(crewCount, 400,600)
    love.graphics.print(crewPosition, 480,600)
    -- love.graphics.print(numOfDice, 480,650)
  end
end
screenWidth = 800
screenHeight = 600
success = love.window.setMode(screenWidth, screenHeight)
enemy = {}
enemiesController = {}
enemiesController.enemies = {}
love.graphics.setDefaultFilter("nearest", "nearest")
enemiesController.image = love.graphics.newImage("enemy.png")

function love.load()
  player = {}
  player.x = 375
  player.y = 550
  player.cooldown = 20
  player.speed = 10
  player.fire_sound = love.audio.newSource("laserSound.wav")
  player.bullets = {}
  player.fire = function()
    if player.cooldown <= 0  then
      love.audio.play(player.fire_sound)
      player.cooldown = 20
      bullet = {}
      bullet.x = player.x + 20 --firing bullet from the main cannon (middle of the ship)
      bullet.y = player.y
      table.insert(player.bullets, bullet)
    end
  end
  enemiesController:spawnEnemy(50,50)
  enemiesController:spawnEnemy(120,50)
end

function enemiesController:spawnEnemy(x,y)
  enemy = {}
  enemy.x = x
  enemy.y = y
  enemy.speed = 10 
  enemy. bullets = {}
  enemy.cooldown = 20
  table.insert(self.enemies, enemy)
end

function enemy:fire()
  if self.cooldown <= 0  then
    self.cooldown = 20
    bullet = {}
    bullet.x = self.x + 20 --firing bullet from the main cannon (middle of the ship)
    bullet.y = self.y
    table.insert(self.bullets, bullet)
  end
end

function love.update(dt)
  
  player.cooldown = player.cooldown - 1
  --move rectangle to the right and left
  
  if(love.keyboard.isDown("right")) then
    --define boundaries on left and right to not let the player "escape" from the screen
    if(player.x < screenWidth-100) then
      player.x = player.x + player.speed
    end
  elseif(love.keyboard.isDown("left")) then
    if(player.x > 50) then
      player.x = player.x -player.speed
    end
  end

  --move rectangle up and down
  -- if(love.keyboard.isDown("up")) then 
  --   y = y - 1
  -- elseif(love.keyboard.isDown("down")) then
  --   y = y + 1
  -- end

  --shoot
  if(love.keyboard.isDown(" ")) then
    player.fire()
  end
  
  --move bullets
  for i, b in ipairs(player.bullets) do
    
    --destroy bullet after reaches top of the screen
    if(b.y < -10) then
      table.remove(player.bullets, i)
    end
    --move bullet 5px up
    b.y = b.y - 5
  end

  --move enemies
  for _, e in pairs(enemiesController.enemies) do
    e.y = e.y + 0.5
  end
  
end

function love.draw()
  -- love.graphics.print("Oi Oi Oi")
  -- love.grapchis.scale(5)
  love.graphics.print(player.x)
  love.graphics.setColor(0, 0, 255)
  love.graphics.rectangle("fill", player.x, player.y, 50, 30)
  -- love.graphics.rectangle("fill", 10, 60 , 100, 20)

  --create bullets
  for _, b in pairs(player.bullets) do
    love.graphics.setColor(255,255,255)
    love.graphics.rectangle("fill", b.x, b.y, 10, 10)
  end

  -- create enemy
  for _,e in pairs(enemiesController.enemies) do
    love.graphics.setColor(255,255,255)
    love.graphics.draw(enemiesController.image, e.x, e.y, 0, 2)
  end
end